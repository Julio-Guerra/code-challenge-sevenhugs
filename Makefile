test/cycle-0/expected := 16
test/cycle-1/expected := 18
test/cycle-2/expected := 21
test/cycle-3/expected := 30
test/codingame-1/expected := 7
test/codingame-2/expected := 3935
test/codingame-3/expected := 15
test/codingame-4/expected := 15000
test/codingame-5/expected := 4999975000
test/codingame-6/expected := 89744892565569
test/hard-0/expected := 8974489271113753
test/hard-1/expected := 89744892714152289
tests := $(patsubst test/%/expected,%,$(filter test/%/expected,$(.VARIABLES)))

.PHONY: all
all: roller

roller: src/main.c
	gcc -std=c11 -Wall -Wextra -O3 -DNDEBUG -o $@ $^

.PHONY: test
test: roller $(addprefix test/, $(tests))

test/%:
	@echo -n "$@ " && result=$$(./roller < tests/$*) && [ $$result = "$($@/expected)" ] && echo ok || ( echo ko got $$result expected $($@/expected) && exit 1 )

dockerized-%:
	docker build --rm --no-cache --build-arg=target=$* .
