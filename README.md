# About

This is my submission to the following Sevenhugs home test assignment. It
contains the source code of a C program called `roller`. The repository includes
everything required to test it in a dockerized environment.

This README file explains how the submission works, what is inside the
repository.

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [About](#about)
- [Assignment](#assignment)
- [Content](#content)
- [Development](#development)
    - [Requirements](#requirements)
    - [Continuous integration pipeline](#continuous-integration-pipeline)
    - [Testing](#testing)
- [Design Decisions](#design-decisions)
    - [Development environment](#development-environment)
    - [Implementation](#implementation)

<!-- markdown-toc end -->


# Assignment

CodinGame Roller Coaster Puzzle: https://www.codingame.com/ide/puzzle/roller-coaster

> 	The Goal
> You have recently been assigned to a new amusement park’s center of analysis and supervision. Your mission is to estimate each day what the earnings will be for each ride that day. You start by looking at the roller coaster.
>  	Rules
> You notice that people like the roller coaster so much that as soon as they have finished a ride, they cannot help but go back for another one.
> People queue up in front of the attraction
> They can either be alone or in a group. When groups are in the queue, they necessarily want to ride together, without being separated.
> People never overtake each other in the queue.
> When there isn’t enough space in the attraction for the next group in the queue, the ride starts (so it is not always full).
> As soon as the ride is finished, the groups that come out, go back into the queue in the same order.
> The attraction contains a limited number L of places.
> The attraction can only function C number of times per day.
> The queue contains a number N of groups.
> Each group contains a number Pi of people.
> Each person spends 1 dirham per ride.
> 
>  	Example
> With L=3, C=3 and 4 groups (N=4) of the following sizes [3,1,1,2]:
> 
> Ride 1: for the first roller coaster ride, only the first group can get on and takes all the places. At the end of the ride, this group returns to the back of the queue that now looks as follows [1,1,2,3].
> Earnings of the ride : 3 dirhams.
> 
> Ride 2 : on the second ride, the following two single-person groups can get on, leaving one place empty (the group of 2 people that follows cannot be separated) At the end of the ride, they return to the back of the queue: [2,3,1,1]
> Earnings of the ride : 2 dirhams.
> 
> Ride 3: for the last ride (C=3), only the group of 2 people can get on, leaving one place empty. Earnings of the ride : 2 dirhams.
> 
> Total earnings: 3+2+2 = 7 dirhams

# Content

```
.
├── Dockerfile
├── Makefile
├── README.md
├── .gitlab-ci.yml
├── src
│   └── main.c
└── tests
```

- Makefile: set of compilation and test recipes, optionally using Docker and the
  Dockerfile.
- Dockerfile: simple dockerized compilation and testing environments.
- .gitlab-ci.yml: simple CI pipeline for GitLab.
- src/: source files.
  - main.c: C language source code of the solution.
- tests/: test input files.

# Development

## Requirements

Everything is "containerized" and simply executed from a Makefile. Which
restricts the required tools to:

- GNU Make
- Docker

## Continuous integration pipeline

File `.gitlab-ci.yml` defines a simple GitLab CI pipeline with just a single
stage compiling the program and running the tests. To reuse it, simply push it
to your gitlab.com repository and it should automatically detect the file and
use it. Note that it uses a custom docker image I modified to include GNU Make
in it.

## Testing

Run `make dockerized-test` to compile the program and test it in the docker
environment:

```console
$ make dockerized-test
docker build --rm --no-cache --build-arg=target=test .
Sending build context to Docker daemon  210.4kB
Step 1/5 : FROM gcc:8.2
 ---> 1d6ec261d687
Step 2/5 : COPY . /usr/src/roller
 ---> a9b1ea88f229
Step 3/5 : WORKDIR /usr/src/roller
 ---> Running in 1b505f176b5a
Removing intermediate container 1b505f176b5a
 ---> 36c9f728620e
Step 4/5 : ARG target
 ---> Running in c1af9e3a3a7e
Removing intermediate container c1af9e3a3a7e
 ---> 7b43213318f2
Step 5/5 : RUN make $target
 ---> Running in db9e0498cfc8
test/codingame-5 ok
test/codingame-1 ok
test/hard-0 ok
test/codingame-4 ok
test/hard-1 ok
test/cycle-3 ok
test/cycle-0 ok
test/codingame-6 ok
test/cycle-1 ok
test/cycle-2 ok
test/codingame-2 ok
test/codingame-3 ok
Removing intermediate container db9e0498cfc8
 ---> b7c175cada42
Successfully built b7c175cada42
```

You can optionally use `make test` to compile and run tests without using
docker, but it will require every tool involved (GCC and sh).

# Design Decisions

## Development environment

The development environment was designed to be simple, i.e. only with the
necessary tools and using them the simplest way:

- Here, the compilation and tests are run in a docker build container. For
  bigger projects, I rather mount the source tree as a container volume. It
  allows faster compilation time (files are no longer copied) and outputs
  everything in-place.  

- Here, the use of docker is explicit in the Makefile target name. For bigger
  projects, I rather recursively call the Makefile (e.g. make target => docker
  exec … make target => target recipe) to make the Makefile the single source of
  truth and to make benefit from its file-dependency management and parallel
  compilation features. I avoided it here as it requires [extra Makefile
  hacks](https://github.com/farjump/Makefile.in/blob/dev/src/mk/toolchain/docker.mk).

## Implementation

The implementation is pretty straightforward and detects the cycle to optimize
the program by adding to the Roller Coaster earnings computed so far, N times
the cycle plus the remaining.

