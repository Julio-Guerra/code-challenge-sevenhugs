FROM gcc:8.2
COPY . /usr/src/roller
WORKDIR /usr/src/roller
ARG target
RUN make $target
