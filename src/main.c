#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <errno.h>
#include <stdint.h>
#include <assert.h>

/**
 * Helper macro to check the return code of a call to `scanf()`.
 * \param RC The return code of the call to `scanf()`.
 * \param NB_INPUTS The number of inputs expected with the call to `scanf()`.
 */
#define CHECK_SCANF_RC(RC, NB_INPUTS)                 \
  do {                                                \
    if ((RC) == EOF)                                  \
      panic("scanf(): %s", strerror(errno));          \
    else if ((RC) != (NB_INPUTS))                     \
      panic("scanf(): unexpected number of inputs");  \
  } while (0)

/**
 * Structure to store individual groups and to regroup them when possible,
 * therefore storing the next group index.
 * For example:
 * {1, 1, 2, 3} with a roller coaster capacity of 3 will result in {2, 2, 3}.
 * The structure allows to store this data in members `next` and
 * `regrouping_size`.
 */
typedef struct {
  /** Number of people in this group, without regrouping. */
  uint32_t nb_people;

  /**
   * Next group index when regrouping. Initialized with value `-1`. A cycle is
   * detected when starting from this group and the value is greater than -1.
   */
  int32_t  next;

  /** Earnings up to this group. */
  uint32_t earnings;

  /** The turn when this group was last seen. */
  uint32_t last_seen_turn;

  /** Group size when regrouping starting from this group. */
  uint32_t regrouping_size;
} t_group;

// Forward declarations. See below definitions for documentation.
// {
static inline void panic(const char* fmt, ...);

static inline t_group* read_input(uint32_t* capacity,
                                  uint64_t* nb_turns,
                                  uint32_t* nb_groups);

static inline uint32_t regroup(t_group  groups[],
                               uint32_t nb_groups,
                               uint32_t start,
                               uint32_t capacity);

static inline uint64_t cycle_earnings(t_group* groups,
                                      uint32_t i,
                                      uint64_t earnings,
                                      uint32_t turn,
                                      uint64_t nb_turns);
// }

int main(void)
{
  uint32_t capacity;
  uint64_t nb_turns;
  uint32_t nb_groups;
  t_group* groups = read_input(&capacity, &nb_turns, &nb_groups);

  // Compute the earnings until there is a cycle (next != -1) or the number of
  // turns `nb_turns` has been reached.
  uint64_t earnings = 0;
  uint32_t i = 0;
  uint32_t turn = 0;
  for (turn = 0; turn < nb_turns && groups[i].next == -1; ++turn)
  {
    uint32_t start = i;
    // Regroup from `start` up to `capacity` people.
    i = regroup(groups, nb_groups, start, capacity);
    // Include this group to the earnings.
    earnings += groups[start].regrouping_size;
    // Used below to compute results with cycles:
    // - Save the earnings of so far.
    groups[start].earnings = earnings;
    // - Current turn.
    groups[start].last_seen_turn = turn;
  }

  // Some turns are still left and there is a cycle.
  if (turn < nb_turns)
    earnings = cycle_earnings(groups, i, earnings, turn, nb_turns);

  printf("%lu\n", earnings);
  exit(EXIT_SUCCESS);
}

/** Initialize a group to its zero-value. */
static void group_init(t_group* group)
{
  assert(group != NULL);
  memset(group, 0, sizeof (*group));
  group->next = -1;
}

/**
 * Regroup groups starting from `start`, up to `capacity`.
 * \param [in,out] groups Array of groups to regroup.
 * \param [in] nb_groups Number of groups in the group array.
 * \param [in] start Regrouping starting index.
 * \param [in] capacity Regrouping capacity limit.
 * \return The index where the regrouping stopped, either by reaching the
 * capacity or by looping in the queue (i.e. start == i).
 */
static uint32_t regroup(t_group  groups[],
                        uint32_t nb_groups,
                        uint32_t start,
                        uint32_t capacity)
{
  uint32_t i = start;
  // The number of regrouped people.
  uint32_t nb_people = 0;

  while (nb_people + groups[i].nb_people <= capacity)
    {
      nb_people += groups[i].nb_people;
      // Next group in the queue, modulo the queue size.
      i = (i + 1) % nb_groups;
      // Break the loop when we looped through every possible group, i.e. they
      // all fit in the the roller coaster.
      if (i == start)
        break;
    }
  groups[start].next = i;
  groups[start].regrouping_size = nb_people;
  return i;
}


/**
 * Compute the earnings with a cycle starting from `i`.
 * \param [in,out] groups Array of groups.
 * \param [in] i Cycle starting index.
 * \param [in] earnings Earnings so far, i.e. up to `turn`, including the cycle
 * and its tail.
 * \param [in] turn Current turn.
 * \param [in] nb_turns Number of turns to perform.
 * \return Earnings up to `nb_turns`.
 */
static uint64_t cycle_earnings(t_group* groups,
                               uint32_t i,
                               uint64_t earnings,
                               uint32_t turn,
                               uint64_t nb_turns)
{
  // Earnings computed so far may include a tail before a cycle:
  // o -> o -> o -> o -> o
  //           ^---------'
  // We can compute earnings of the tail based on previously saved data:
  uint64_t cycle_tail_earnings = 0;
  cycle_tail_earnings = groups[i].earnings - groups[i].regrouping_size;
  // Hence the total earnings of the cycle:
  uint64_t cycle_earnings = earnings - cycle_tail_earnings;
  // Number of turns in the cycle.
  uint32_t cycle_turns = turn - groups[i].last_seen_turn;
  // Number of turns still left.
  nb_turns -= turn;
  // Hence the number of cycles to be done.
  uint32_t nb_cycles = nb_turns / cycle_turns;
  // Hence the corresponding earnings: number of cycles times the earnings per
  // cycle.
  earnings += nb_cycles * cycle_earnings;
  // Remaing of previous division of the number of cycles to be done.
  uint32_t cycle_remaining = nb_turns % cycle_turns;
  if (cycle_remaining > 0)
  {
    // Get the index in cycle where it stops.
    for (; cycle_remaining > 1; --cycle_remaining)
      i = groups[i].next;
    // Update the earings the previously computed earings up to this group,
    // minus the tail.
    earnings += groups[i].earnings - cycle_tail_earnings;
  }
  return earnings;
}

/**
 * Read the input data from standard input and return the parameters.
 * \param [out] capacity Roller coaster capacity.
 * \param [out] nb_turns Number of roller coaster turns.
 * \param [out] nb_groups Number of returned groups.
 * \return The array of groups.
 */
static t_group* read_input(uint32_t* capacity,
                           uint64_t* nb_turns,
                           uint32_t* nb_groups)
{
  assert(capacity != NULL && nb_turns != NULL && nb_groups != NULL);
  int rc = scanf("%u%lu%u", capacity, nb_turns, nb_groups);
  CHECK_SCANF_RC(rc, 3);

  if (*capacity == 0 || *nb_turns == 0 || *nb_groups == 0)
    panic("unexpected parameters");

  t_group* groups = (t_group*) malloc(*nb_groups * sizeof (t_group));
  if (groups == NULL)
    panic("malloc(): unexpected null pointer returned");

  for (uint32_t i = 0; i < *nb_groups; ++i)
  {
    group_init(&groups[i]);
    rc = scanf("%u", &groups[i].nb_people);
    CHECK_SCANF_RC(rc, 1);
    groups[i].next = -1;
  }
  return groups;
}

/**
 * Helper function to print an error message and exit the program.
 */
static void panic(const char* fmt, ...)
{
  printf("Panic: ");
  va_list args;
  va_start(args, fmt);
  vprintf(fmt, args);
  va_end(args);
  printf("\n");
  exit(EXIT_FAILURE);
}
